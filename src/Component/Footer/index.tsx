import React from 'react'

interface Props {

}

const Footer = (props: Props) => {
    return (
        <section className="w3l-footer-29-main" id="footer">
            <div className="footer-29 text-center editContent">
                <div className="container">
                    <div className="main-social-footer-29">
                        <a href="#facebook" className="facebook editContent" style={{ outline: "none", cursor: "inherit" }}><span className="fa fa-facebook" style={{ outline: "none", cursor: "inherit" }}></span></a>
                        <a href="#twitter" className="twitter editContent" style={{ outline: "none", cursor: "inherit" }}><span className="fa fa-twitter" style={{ outline: "none", cursor: "inherit" }}></span></a>
                        <a href="#instagram" className="instagram editContent" style={{ outline: "none", cursor: "inherit" }}><span className="fa fa-instagram" style={{ outline: "none", cursor: "inherit" }}></span></a>
                        <a href="#google-plus" className="google-plus editContent" style={{ outline: "none", cursor: "inherit" }}><span className="fa fa-google-plus" style={{ outline: "none", cursor: "inherit" }}></span></a>
                        <a href="#linkedin" className="linkedin editContent" style={{ outline: "none", cursor: "inherit" }}><span className="fa fa-linkedin" style={{ outline: "none", cursor: "inherit" }}></span></a>
                    </div>
                    <div className="bottom-copies text-center">
                        <p className="copy-footer-29">© 2020 My Website. All rights reserved | Designed by <a href="https://w3layouts.com">W3layouts</a></p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export  default Footer;