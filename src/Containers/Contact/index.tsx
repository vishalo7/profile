import React from 'react'

interface Props {

}

 const Contact = (props: Props) => {
    return (
        <div id="page" className="page">

            {/* <!-- Headers-4 block --> */}


            {/* <!-- Headers-4 block --> */}{/* <!--  Main banner section --> */}

            {/* <!--  //Main banner section --> */}{/* <!-- grids block 5 --> */}
            {/* <!-- Headers-4 block --> */}


            {/* <!-- Headers-4 block --> */}{/* <!-- w3l-content-photo-5 --> */}

            {/* <!-- //w3l-content-photo-5 --> */}{/* <!-- home page service grids --> */}

            {/* <!-- //home page service grids --> */}{/* <!-- specifications --> */}
            {/* <!-- grids block 5 --> */}
            {/* <!-- Headers-4 block --> */}


            {/* <!-- Headers-4 block --> */}{/* <!-- grids block 5 --> */}
            {/* <!-- Headers-4 block --> */}

            {/* <!-- Headers-4 block --> */}{/* <!-- contact --> */}
            <section className="w3l-contacts-12" id="contact">
                <div className="contact-top pt-5">
                    <div className="container py-md-3">

                        <div className="row cont-main-top">
                            {/* <!-- contact form --> */}
                            <div className="contacts12-main col-lg-6 ">

                                <form action="https://sendmail.w3layouts.com/submitForm" method="post" className="main-input">
                                    <div className="top-inputs d-grid editContent">
                                        <input type="text" placeholder="Name" name="w3lName" id="w3lName" />

                                    </div>
                                    <div className="editContent">
                                        <input type="email" name="email" placeholder="Email" id="w3lSender" />
                                    </div>
                                    <div className="editContent">
                                        <textarea placeholder="Message" name="w3lMessage" id="w3lMessage" ></textarea>
                                    </div>
                                    <div className="text-right">
                                        <button type="submit" className="btn btn-theme2">Submit Now</button>
                                    </div>
                                </form>
                            </div>
                            {/* <!-- //contact form --> */}
                            {/* <!-- contact address --> */}
                            <div className="contact col-lg-6 mt-lg-0 mt-5 editContent" style={{ outline: "none", cursor: "inherit" }}>
                                <div className="cont-subs">
                                    <div className="cont-add editContent" style={{ outline: "none", cursor: "inherit" }}>
                                        <div className="cont-add-lft">
                                            <span className="fa fa-map-marker" aria-hidden="true" style={{ outline: "none", cursor: "inherit" }}></span>
                                        </div>
                                        <div className="cont-add-rgt">
                                            <p className="contact-text-sub editContent" style={{ outline: "none", cursor: "inherit" }}>PO Box 1212, London, UK</p>
                                        </div>

                                    </div>
                                    <div className="cont-add add-2 editContent" style={{ outline: "none", cursor: "inherit" }}>
                                        <div className="cont-add-lft">
                                            <span className="fa fa-envelope" aria-hidden="true" style={{ outline: "none", cursor: "inherit" }}></span>
                                        </div>
                                        <div className="cont-add-rgt">
                                            <a className="editContent" href="mailto:info@example.com" style={{ outline: "none", cursor: "inherit" }}>
                                                <p className="contact-text-sub editContent" style={{ outline: "none", cursor: "inherit" }}>info@example.com</p>
                                            </a>
                                        </div>

                                    </div>
                                    <div className="cont-add editContent" style={{ outline: "none", cursor: "inherit" }}>
                                        <div className="cont-add-lft">
                                            <span className="fa fa-phone" aria-hidden="true" style={{ outline: "none", cursor: "inherit" }}></span>
                                        </div>
                                        <div className="cont-add-rgt">
                                            <a className="editContent" href="tel:+7-800-999-800" style={{ outline: "none", cursor: "inherit" }}>
                                                <p className="contact-text-sub editContent" style={{ outline: "none", cursor: "inherit" }}>+7-800-999-800</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="cont-add add-3 editContent" style={{ outline: "none", cursor: "inherit" }}>
                                        <div className="cont-add-lft">
                                            <span className="fa fa-file-pdf-o" aria-hidden="true" style={{ outline: "none", cursor: "inherit" }}></span>
                                        </div>
                                        <div className="cont-add-rgt">
                                            <a className="editContent" href="#" style={{ outline: "none", cursor: "inherit" }}>
                                                <p className="contact-text-sub editContent" style={{ outline: "none", cursor: "inherit" }}>Download Resume</p>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            {/* <!-- //contact address --> */}

                        </div>
                    </div>
                    {/* <!-- map --> */}
                    <div className="map editContent" style={{ outline: "none", cursor: "inherit" }}>
                        {/* <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d158857.7281066703!2d-0.24168144921176335!3d51.5287718408761!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C%20UK!5e0!3m2!1sen!2sin!4v1569921526194!5m2!1sen!2sin" frameborder="0" style="border:0;" allowfullscreen=""></iframe> */}
                    </div>
                    {/* <!-- //map --> */}
                </div>
            </section>
            {/* <!-- //contact --> */}{/* <!-- grids block 5 --> */}
        </div>


    )
}
export  default Contact;