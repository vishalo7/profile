const userDetails = {
    userName: 'vishal gohel',
    userTitle: "",
    userSubTitle: '',
    address: '',
    email: '',
    phone: '',
    cv: '',

    skill: [{
        icon: 'fa fa-code',
        skillName: 'Front End',
        skillDec: 'Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt '
    },
    {
        icon: 'fa fa-database',
        skillName: 'Back End',
        skillDec: 'Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt '
    },
    {
        icon: 'fa fa fa-cogs',
        skillName: 'Test Case',
        skillDec: 'Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do eiusmod tempor incididunt '
    }],
    education: [{
        clgName: '',
        clgDec: '',
        clgYer: ''
    }],
    hobbies: [{
        hobName: 'criket',
        hobDec: '',
        hobIcon: 'fa fa-code'
    },{
        hobName: 'game',
        hobDec: '',
        hobIcon: 'fa fa-code'
    },{
        hobName: 'motivation',
        hobDec: '',
        hobIcon: 'fa fa-code'
    },{
        hobName: 'Photo graphish',
        hobDec: '',
        hobIcon: 'fa fa-code'
    },
],
    skillList: [{
        skillName: 'React js',
        parangat: 90,
    },
    {
        skillName: 'React Native',
        parangat: 90,
    },
    {
        skillName: 'React Hooks',
        parangat: 90,
    }
        ,
    {
        skillName: 'Redux',
        parangat: 90,
    }
        ,
    {
        skillName: 'DND',
        parangat: 25,
    },
    {
        skillName: 'Node js',
        parangat: 90,
    },
    {
        skillName: 'MongoDb',
        parangat: 25,
    }],
    social: [{
        sName: ''
    }]
};
export default userDetails;